﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Maze
{
    /// <summary>
    /// Interaction logic for MazeItem.xaml
    /// </summary>
    public partial class MazeItem : UserControl
    {
        public enum ItemTypeEnum
        {
            Open = 0,
            Wall = 1,
            Start = 2,
            Finish = 3
        }

        public enum ItemStatusEnum
        {
            NotVisited,
            Visited
        }

        public ItemTypeEnum ItemType { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        private int _itemType;
        
        public Button UserControlButton
        {
            get { return this._mazeItem; }
        }

        public MazeItem()
        {
            InitializeComponent();
            this.ItemType = ItemTypeEnum.Open;
            this._mazeItem.Content = this.ItemType.ToString();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            _itemType++;
            _itemType = _itemType % 4;
            this.ItemType = (ItemTypeEnum)Enum.Parse(typeof(ItemTypeEnum), _itemType.ToString());
            this._mazeItem.Content = this.ItemType.ToString();
            //this.mazeItem.Content = this.X + "," + this.Y;
            SetItemColor();
        }

        /// <summary>
        /// 
        /// </summary>
        private void SetItemColor()
        { 
            if (this.ItemType == ItemTypeEnum.Wall)
                this._mazeItem.Background = Brushes.Red;
            if (this.ItemType == ItemTypeEnum.Open)
                this._mazeItem.ClearValue(Button.BackgroundProperty);
            if (this.ItemType == ItemTypeEnum.Start)
                this._mazeItem.Background = Brushes.Green;
            if (this.ItemType == ItemTypeEnum.Finish)
                this._mazeItem.Background = Brushes.Gold;
        }
    }
}
