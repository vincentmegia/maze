﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace Maze
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MazeItem[,] _mazeArray;
        public bool HasStart { get; set; }
        public bool HasFinish { get; set; }
        private bool isEndFound;
        private MazeItem _startItem;
        private MazeItem _finishItem;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            List<MazeItem> mazeItems = ControlHelper.GetLogicalChildCollection<MazeItem>(parentGrid);
            foreach (var item in mazeItems)
            {
                if (item.ItemType != MazeItem.ItemTypeEnum.Start &&
                    item.ItemType != MazeItem.ItemTypeEnum.Finish)
                {
                    item.ItemType = MazeItem.ItemTypeEnum.Open;
                    item.UserControlButton.Content = item.ItemType.ToString();
                    item.UserControlButton.ClearValue(Button.BackgroundProperty);
                    isEndFound = false;
                }
            }
            
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //store the user defined maze in a 2x2 array
            _mazeArray = new MazeItem[6,6];
            GetMazeItems();
            SearchStartAndFinish();
            //SolvePuzzle(this._startItem, _mazeArray);
        }

        private void start_Loaded(object sender, RoutedEventArgs e)
        {
            this.start.ItemType = MazeItem.ItemTypeEnum.Start;
            this.start.UserControlButton.Content = Maze.MazeItem.ItemTypeEnum.Start.ToString();
            this.HasStart = true;
        }

        private void finish_Loaded(object sender, RoutedEventArgs e)
        {
            this.finish.ItemType = MazeItem.ItemTypeEnum.Finish;
            this.finish.UserControlButton.Content = Maze.MazeItem.ItemTypeEnum.Finish.ToString();
            this.HasFinish = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private void GetMazeItems()
        {
            List<MazeItem> mazeItems = ControlHelper.GetLogicalChildCollection<MazeItem>(parentGrid);
            foreach (var item in mazeItems)
            {
                _mazeArray[item.X, item.Y] = item;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void SearchStartAndFinish()
        {
            foreach (var item in _mazeArray)
            {
                if (item == null) continue;
                if (item.ItemType == MazeItem.ItemTypeEnum.Start)
                {
                    this._startItem = item;
                    continue;
                }
                if (item.ItemType == MazeItem.ItemTypeEnum.Finish)
                {
                    this._finishItem = item;
                    continue;
                }
            }
        }

       
    }
}
