﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Maze
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        private int _columnCount = 10;
        private int _rowCount = 10;
        private MazeItem[,] _mazeArray;
        
        private bool _debugGrid;

        public Window1()
        {
            InitializeComponent();
            BuildPuzzle();
        }

        /// <summary>
        /// 
        /// </summary>
        private void BuildPuzzle()
        {
            //sample create a 10x10 grid.
            CreateColumnDefinitions();
            //CreateRowDefinitions();
            CreateMazeItems();
            _debugGrid = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private void CreateColumnDefinitions()
        {
            var columnsToCreate = Math.Abs(_columnCount - _puzzleGrid.ColumnDefinitions.Count);
            for (var index = 0; index <= columnsToCreate; index++)
            {
                _puzzleGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void CreateMazeItems()
        { 
            for (var columnIndex = _columnCount - 1; columnIndex >= 0; columnIndex--)
            {
                var stackPanel = new StackPanel();
                for (var rowIndex = _rowCount - 1; rowIndex >= 0; rowIndex--)
                {
                    var newMazeItem = new MazeItem { X = columnIndex, Y = rowIndex };
                    newMazeItem.UserControlButton.Content = columnIndex + "," + rowIndex;
                    stackPanel.Children.Add(newMazeItem);
                    Grid.SetColumn(stackPanel, columnIndex);
                    Grid.SetRow(stackPanel, 0);
                }
                _puzzleGrid.Children.Add(stackPanel);
            }
        }

        private void _reset_Click(object sender, RoutedEventArgs e)
        {
            List<MazeItem> mazeItems = ControlHelper.GetLogicalChildCollection<MazeItem>(_puzzleGrid);
            foreach (var item in mazeItems)
            {
                item.ItemType = MazeItem.ItemTypeEnum.Open;
                item.UserControlButton.Content = item.ItemType.ToString();
                item.UserControlButton.Content = item.X + "," + item.Y;
                item.UserControlButton.ClearValue(Button.BackgroundProperty);
            }
        }

        private void _search_Click(object sender, RoutedEventArgs e)
        {
            _mazeArray = new MazeItem[_columnCount, _rowCount];
            GetMazeItems();
            try
            {
                new PuzzleService(_mazeArray, this).SolvePuzzle();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void _readme_Click(object sender, RoutedEventArgs e)
        {
            string message = "The program is a simple puzzle solve using A* recursion logic.\n";
            message += "By clicking a coordinate button you can choose from the following:\n";
            message += "1: Open = Open path\n";
            message += "2: Wall = Wall, dead end and program will take a different route\n";
            message += "3: Start = Starting point of the puzzle\n";
            message += "4: Finish = End point of the puzzle\n";
            message += "\n";
            message += "Please avoid having more than 1 Start or Finish";
            MessageBox.Show(message);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private void GetMazeItems()
        {
            List<MazeItem> mazeItems = ControlHelper.GetLogicalChildCollection<MazeItem>(_puzzleGrid);
            foreach (var item in mazeItems)
            {
                _mazeArray[item.X, item.Y] = item;
            }
        }
    }
}
