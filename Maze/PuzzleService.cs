﻿using System.Linq;
using System.Collections.Generic;
using System.Windows.Media;
using System;

namespace Maze
{
    public class PuzzleService
    {
        public bool IsFinishFound { get; set; }
        private MazeItem[,] _mazeArray;
        private Window1 _parent;

        public PuzzleService(MazeItem[,] mazeArray, Window1 parent)
        {
            _mazeArray = mazeArray;
            _parent = parent;
        }

        /// <summary>
        /// 
        /// </summary>
        public void SolvePuzzle()
        {
            MazeItem start = GetStart();
            MazeItem finish = GetFinish();
            SolvePuzzle(start);
        }

        /// <summary>
        /// The idea is to check nearby path by clockwise pattern
        /// </summary>
        /// <param name="item"></param>
        /// <param name="_mazeArray"></param>
        public void SolvePuzzle(MazeItem item)
        {
            if ((item.ItemType == MazeItem.ItemTypeEnum.Open ||
                item.ItemType == MazeItem.ItemTypeEnum.Start) &&
                item.UserControlButton.Content.ToString() != "Visited")
            {
                if (item.ItemType != MazeItem.ItemTypeEnum.Start &&
                    item.ItemType != MazeItem.ItemTypeEnum.Finish)
                {
                    item.UserControlButton.Content = "Visited";
                    item.UserControlButton.Background = Brushes.Green;
                }
                var nextItem = new MazeItem();
                //check 12 direction

                if ((_mazeArray.GetLength(0) > item.X + 1 &&
                    _mazeArray.GetLength(1) > item.Y) && !IsFinishFound)
                {
                    nextItem = _mazeArray[item.X + 1, item.Y];
                    SolvePuzzle(nextItem);
                }
                //check 3 direction
                if ((_mazeArray.GetLength(0) > item.X) &&
                    _mazeArray.GetLength(1) > item.Y + 1 && !IsFinishFound)
                {
                    nextItem = _mazeArray[item.X, item.Y + 1];
                    SolvePuzzle(nextItem);
                }
                //check 6 direction
                if ((_mazeArray.GetLength(0) > item.X - 1) &&
                    _mazeArray.GetLength(1) > item.Y && !IsFinishFound)
                {
                    nextItem = _mazeArray[item.X - 1, item.Y];
                    SolvePuzzle(nextItem);
                }
                //check 9 direction
                if ((_mazeArray.GetLength(0) > item.X) &&
                    _mazeArray.GetLength(1) > item.Y - 1 && !IsFinishFound)
                {
                    nextItem = _mazeArray[item.X, item.Y - 1];
                    SolvePuzzle(nextItem);
                }
                //check 1 direction
                if ((_mazeArray.GetLength(0) > item.X + 1) &&
                    _mazeArray.GetLength(1) > item.Y + 1 && !IsFinishFound)
                {
                    nextItem = _mazeArray[item.X + 1, item.Y + 1];
                    SolvePuzzle(nextItem);
                }

                //check 5 direction
                if ((_mazeArray.GetLength(0) > item.X - 1) &&
                    _mazeArray.GetLength(1) > item.Y + 1 && !IsFinishFound)
                {
                    nextItem = _mazeArray[item.X - 1, item.Y + 1];
                    SolvePuzzle(nextItem);
                }

                //check 7 direction
                if ((_mazeArray.GetLength(0) > item.X - 1) &&
                    _mazeArray.GetLength(1) > item.Y - 1 && !IsFinishFound)
                {
                    nextItem = _mazeArray[item.X - 1, item.Y - 1];
                    SolvePuzzle(nextItem);
                }

                //check 11 direction
                if ((_mazeArray.GetLength(0) > item.X + 1) &&
                    _mazeArray.GetLength(1) > item.Y - 1 && !IsFinishFound)
                {
                    nextItem = _mazeArray[item.X + 1, item.Y - 1];
                    SolvePuzzle(nextItem);
                }
            }
            if (item.ItemType == MazeItem.ItemTypeEnum.Finish)
            {
                IsFinishFound = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public MazeItem GetStart()
        {
            List<MazeItem> mazeItems = ControlHelper.GetLogicalChildCollection<MazeItem>(_parent._puzzleGrid);
            var start = from mazeItem in mazeItems
                        where mazeItem.ItemType == MazeItem.ItemTypeEnum.Start
                        select mazeItem;
            if (start.Any())
                if (start.Count() > 1)
                    throw new Exception("Cannot have more than 1 \"Finish\", please check the grid and remove the duplicate(s) ");
                else
                    return start.First();
            else
                return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public MazeItem GetFinish()
        {
            List<MazeItem> mazeItems = ControlHelper.GetLogicalChildCollection<MazeItem>(_parent._puzzleGrid);
            var finish = from mazeItem in mazeItems
                         where mazeItem.ItemType == MazeItem.ItemTypeEnum.Finish
                         select mazeItem;
            if (finish.Any())
            {
                if (finish.Count() > 1)
                    throw new Exception("Cannot have more than 1 \"Finish\", please check the grid and remove the duplicate(s) ");
                else
                    return finish.First();
            }
                
            else
                return null;
        }
    }
}
